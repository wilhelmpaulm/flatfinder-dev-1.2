require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  test "should get profile" do
    get users_profile_url
    assert_response :success
  end

  test "should get bookmarks" do
    get users_bookmarks_url
    assert_response :success
  end

  test "should get reservations" do
    get users_reservations_url
    assert_response :success
  end

  test "should get units" do
    get users_units_url
    assert_response :success
  end

  test "should get payments" do
    get users_payments_url
    assert_response :success
  end

end
