class CreateUnits < ActiveRecord::Migration[5.0]
  def change
    create_table :units do |t|
      t.string :status
      t.string :category
      t.string :name
      t.text :description
      t.float :size
      t.float :monthly_price
      t.float :weekly_price
      t.float :deposit_price
      t.string :city
      t.text :address
      t.integer :min_capacity
      t.integer :max_capacity
      t.integer :showers
      t.integer :sinks
      t.integer :toilets
      t.integer :urinals
      t.integer :bathtubs
      t.integer :kitchens
      t.integer :bedrooms
      t.integer :single_beds
      t.integer :bunk_beds
      t.date :date_available
      t.integer :min_weeks
      t.text :notes
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
