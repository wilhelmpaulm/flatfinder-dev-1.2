class CreateJoinTableUserLandmark < ActiveRecord::Migration[5.0]
  def change
    create_join_table :users_landmarks, id: false  do |t|
      t.belongs_to :user, index: true
      t.belongs_to :landmark, index: true
    end

    create_join_table :users_amenity, id: false  do |t|
      t.belongs_to :user, index: true
      t.belongs_to :amenity, index: true
    end
  end
end
