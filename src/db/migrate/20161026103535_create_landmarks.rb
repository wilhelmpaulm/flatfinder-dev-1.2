class CreateLandmarks < ActiveRecord::Migration[5.0]
  def change
    create_table :landmarks do |t|
      t.string :name
      t.text :description
      t.string :icon
      t.text :image_url

      t.timestamps
    end
  end
end
