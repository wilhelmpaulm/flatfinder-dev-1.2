# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161026132901) do

  create_table "amenities", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.string   "icon"
    t.text     "image_url"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "bookmarks", force: :cascade do |t|
    t.integer  "unit_id"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["unit_id"], name: "index_bookmarks_on_unit_id"
    t.index ["user_id"], name: "index_bookmarks_on_user_id"
  end

  create_table "landmarks", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.string   "icon"
    t.text     "image_url"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "units", force: :cascade do |t|
    t.string   "status"
    t.string   "category"
    t.string   "name"
    t.text     "description"
    t.float    "size"
    t.float    "monthly_price"
    t.float    "weekly_price"
    t.float    "deposit_price"
    t.string   "city"
    t.text     "address"
    t.integer  "min_capacity"
    t.integer  "max_capacity"
    t.integer  "showers"
    t.integer  "sinks"
    t.integer  "toilets"
    t.integer  "urinals"
    t.integer  "bathtubs"
    t.integer  "kitchens"
    t.integer  "bedrooms"
    t.integer  "single_beds"
    t.integer  "bunk_beds"
    t.date     "date_available"
    t.integer  "min_weeks"
    t.text     "notes"
    t.integer  "user_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.index ["user_id"], name: "index_units_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",     null: false
    t.string   "encrypted_password",     default: "",     null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,      null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.string   "role"
    t.string   "last_name"
    t.string   "first_name"
    t.string   "gender",                 default: "name"
    t.date     "birthdate"
    t.string   "mobile"
    t.text     "address"
    t.string   "city"
    t.string   "occupation"
    t.string   "office_name"
    t.text     "office_address"
    t.text     "image_url"
    t.text     "fb_id"
    t.text     "twitter_id"
    t.text     "instagram_id"
    t.text     "notes"
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "users_amenity_{:id=>false}", id: false, force: :cascade do |t|
    t.integer "users_amenity_id", null: false
    t.integer "{:id=>false}_id",  null: false
    t.integer "user_id"
    t.integer "amenity_id"
    t.index ["amenity_id"], name: "index_users_amenity_{:id=>false}_on_amenity_id"
    t.index ["user_id"], name: "index_users_amenity_{:id=>false}_on_user_id"
  end

  create_table "users_landmarks_{:id=>false}", id: false, force: :cascade do |t|
    t.integer "users_landmark_id", null: false
    t.integer "{:id=>false}_id",   null: false
    t.integer "user_id"
    t.integer "landmark_id"
    t.index ["landmark_id"], name: "index_users_landmarks_{:id=>false}_on_landmark_id"
    t.index ["user_id"], name: "index_users_landmarks_{:id=>false}_on_user_id"
  end

end
