Rails.application.routes.draw do

  resources :bookmarks
  resources :units
  resources :landmarks
  resources :amenities

  devise_for :users, :path => '', :controllers => {registrations: 'users/registrations'},
             path_names: {sign_in: 'login', sign_up: 'sign_up', sign_out: 'logout', edit: 'profile'}

  get '/', to: 'static_pages#home'
  get '/about', to: 'static_pages#about'
  get '/contact', to: 'static_pages#contact'
  get '/search', to: 'static_pages#search'
  get '/search/:id', to: 'static_pages#single', as: 'search_single'
  # get '/single', to: 'static_pages#single'
  get '/faqs', to: 'static_pages#faqs'

  get '/users/:id', to: 'users#profile'


  get '/bookmarks', to: 'users#bookmarks'


  root 'static_pages#home'
end
