module ApplicationHelper

  def unit_statuses
    return ['active', 'pending', 'hidden', 'closed']
  end

  def unit_categories
    return ['apartment', 'condo', 'townhouse', 'loft','room']
  end

  def reservation_statuses
    return ['unpaid', 'paid']
  end

  def genders
    return ['male', 'female']
  end

  def cities
    return ['manila', 'makati', 'taguig', 'pasig']
  end
end
