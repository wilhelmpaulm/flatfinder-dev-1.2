//= require jquery/jquery.min
//= require moment
//= require jquery_ujs
//= require bootstrap/bootstrap.min
//= require bootstrap-datetimepicker/bootstrap-datetimejs.js
//= require cleanblog/clean-blog.js
//= require list.min
//= require_self


var ready = function () {

    $('input[type="date"]').datetimepicker({
        format: "YYYY-MM-DD"
    });

    function init_map() {
        var var_location = new google.maps.LatLng(14.2888522, 121.0946385);

        var var_mapoptions = {
            center: var_location,
            zoom: 15
        };

        var var_marker = new google.maps.Marker({
            position: var_location,
            map: var_map,
            title: "Sta Rosa"
        });

        var var_map = new google.maps.Map(document.getElementById("map-container"),
            var_mapoptions);

        var_marker.setMap(var_map);
    }

    function init_map_footer() {
        var var_location = new google.maps.LatLng(14.2888522, 121.0946385);

        var var_mapoptions = {
            center: var_location,
            zoom: 15
        };

        var var_marker = new google.maps.Marker({
            position: var_location,
            map: var_map,
            title: "Sta Rosa"
        });

        var var_map = new google.maps.Map(document.getElementById("map-container-footer"),
            var_mapoptions);

        var_marker.setMap(var_map);
    }

    google.maps.event.addDomListener(window, 'load', init_map);
    google.maps.event.addDomListener(window, 'load', init_map_footer);

    $('[data-toggle="tooltip"]').tooltip();


    var options = {
        valueNames: ['unit-name', 'unit-price', 'unit-location']
    };

    var searchList = new List('units', options);


    $('#myCarousel').carousel({
        interval: 5000
    });

    $('#carousel-text').html($('#slide-content-0').html());

    //Handles the carousel thumbnails
    $('[id^=carousel-selector-]').click(function () {
        var id = this.id.substr(this.id.lastIndexOf("-") + 1);
        var id = parseInt(id);
        $('#myCarousel').carousel(id);
    });


    // When the carousel slides, auto update the text
    $('#myCarousel').on('slid.bs.carousel', function (e) {
        var id = $('.item.active').data('slide-number');
        $('#carousel-text').html($('#slide-content-' + id).html());
    });
}

$(document).ready(ready);
