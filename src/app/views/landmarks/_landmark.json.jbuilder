json.extract! landmark, :id, :name, :description, :icon, :image_url, :created_at, :updated_at
json.url landmark_url(landmark, format: :json)