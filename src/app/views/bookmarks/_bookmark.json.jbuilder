json.extract! bookmark, :id, :unit_id, :user_id, :created_at, :updated_at
json.url bookmark_url(bookmark, format: :json)