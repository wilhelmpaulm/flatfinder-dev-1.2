json.extract! amenity, :id, :name, :description, :icon, :image_url, :created_at, :updated_at
json.url amenity_url(amenity, format: :json)