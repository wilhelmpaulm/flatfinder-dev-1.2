class ErrorsController < ApplicationController
  def error_404
  end

  def error_500
  end

  def error_300
  end
end
