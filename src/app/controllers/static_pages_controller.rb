class StaticPagesController < ApplicationController
  layout 'flatfinder'

  def home
  end

  def about
  end

  def contact
  end

  def search
    @units = Unit.all
  end

  def single
    @unit = Unit.where(id: params[:id]).first
    if (@unit.nil?)
      flash[:alert] = "The unit you are trying to access does not exist"
      redirect_to search_path
    end
  end

  def faqs
  end
end
