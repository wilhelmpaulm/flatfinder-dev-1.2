class UsersController < ApplicationController
  before_action :authenticate_user!
  layout 'flatfinder'

  def profile
  end

  def show
  end

  def bookmarks
  end

  def reservations
  end

  def units
  end

  def payments
  end
end
