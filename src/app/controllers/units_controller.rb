class UnitsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_unit, only: [:show, :edit, :update, :destroy]

  layout 'flatfinder'
  # GET /units
  # GET /units.json

  def index
    @units = current_user.units # Unit.all
  end

  # GET /units/1
  # GET /units/1.json
  def show
    @unit = current_user.units.where(id: params[:id]).first # Unit.all
    if (@unit.nil?)
      flash[:alert] = "The unit you are trying to access does not exist"
      redirect_to units_path
    end
  end

  # GET /units/new
  def new
    @unit = Unit.new
  end

  # GET /units/1/edit
  def edit
  end

  # POST /units
  # POST /units.json
  def create
    @unit = Unit.new(unit_params)
    @unit.user_id = current_user.id
    @unit.save

    respond_to do |format|
      if @unit.persisted?
        format.html { redirect_to @unit, notice: 'Unit was successfully created.' }
        format.json { render :show, status: :created, location: @unit }
      else
        format.html { render :new }
        format.json { render json: @unit.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /units/1
  # PATCH/PUT /units/1.json
  def update
    respond_to do |format|
      if @unit.update(unit_params)
        format.html { redirect_to @unit, notice: 'Unit was successfully updated.' }
        format.json { render :show, status: :ok, location: @unit }
      else
        format.html { render :edit }
        format.json { render json: @unit.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /units/1
  # DELETE /units/1.json
  def destroy
    @unit.destroy
    respond_to do |format|
      format.html { redirect_to units_url, notice: 'Unit was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_unit
    @unit = current_user.units.where(id: params[:id]).first
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def unit_params
    params.require(:unit).permit(:status, :category, :name, :description, :size, :monthly_price, :weekly_price, :deposit_price, :city, :address, :min_capacity, :max_capacity, :showers, :sinks, :toilets, :urinals, :bathtubs, :kitchens, :bedrooms, :single_beds, :bunk_beds, :date_available, :min_weeks, :notes, :user_id)
  end
end
